package org.gamesys.registration.junit;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.gamesys.registration.client.RegistrationClient;
import org.gamesys.registration.service.User;
import org.junit.jupiter.api.Test;

class RegistrationTest1 {

	@Test
	void test() {
		RegistrationClient client = new RegistrationClient();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {
			User user1 = new User("Pratap", "Prapra123", sdf.parse("1980-10-09"), 99892522);// Valid User details

			assertEquals(true, client.register(user1));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // valid details
	}

}
