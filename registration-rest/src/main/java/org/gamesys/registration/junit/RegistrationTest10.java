package org.gamesys.registration.junit;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.gamesys.registration.client.RegistrationClient;
import org.gamesys.registration.service.User;
import org.junit.jupiter.api.Test;

class RegistrationTest10 {

	@Test
	void test() {
		RegistrationClient client = new RegistrationClient();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {
			User user10 = new User("Pratap", "Prapra123", sdf.parse("1980-10-09"), 99892522);// Trying to add already
																								// added user

			assertEquals(false, client.register(user10));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // valid details
	}

}
