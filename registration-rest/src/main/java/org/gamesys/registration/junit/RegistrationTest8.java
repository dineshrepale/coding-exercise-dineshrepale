package org.gamesys.registration.junit;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.gamesys.registration.client.RegistrationClient;
import org.gamesys.registration.service.User;
import org.junit.jupiter.api.Test;

class RegistrationTest8 {

	@Test
	void test() {
		RegistrationClient client = new RegistrationClient();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {
			User user8 = new User("Mark", "Using145", sdf.parse("1984-9-2"), 22211368);// Valid user details.

			assertEquals(true, client.register(user8));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // valid details
	}

}
