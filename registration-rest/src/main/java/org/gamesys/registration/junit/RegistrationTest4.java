package org.gamesys.registration.junit;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.gamesys.registration.client.RegistrationClient;
import org.gamesys.registration.service.User;
import org.junit.jupiter.api.Test;

class RegistrationTest4 {

	@Test
	void test() {
		RegistrationClient client = new RegistrationClient();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {
			User user4 = new User("Wilson", "Logical3", sdf.parse("2019-5-25"), 58659248);// Date of birth of future.

			assertEquals(false, client.register(user4));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // valid details
	}

}
