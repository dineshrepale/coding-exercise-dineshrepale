package org.gamesys.registration.junit;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.gamesys.registration.client.RegistrationClient;
import org.gamesys.registration.service.User;
import org.junit.jupiter.api.Test;

class RegistrationTest9 {

	@Test
	void test() {
		RegistrationClient client = new RegistrationClient();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		try {
			User user9 = new User("John", "England359", sdf.parse("1976-12-14"), 12345678);// ssn details not available with exclusion.

			assertEquals(false, client.register(user9));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // valid details
	}

}
