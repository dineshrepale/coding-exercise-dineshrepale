package org.gamesys.registration.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.gamesys.registration.service.User;
import org.gamesys.registration.util.ResponseCode;
import com.fasterxml.jackson.databind.ObjectMapper;
/*
 * This is a RESTClient, It will make a POST call to registration service with User details in Json format.
 * Response will be predefined code, On the basis of responsecode appropriate message will be populated from enum.
 * Returning boolean is for Junit testing purpose.
 * 
 * */
public class RegistrationClient {

	public static final String REST_URL = "http://localhost:8080/registartion-rest/restapi/register";

	public boolean register(User user) {

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonInString = mapper.writeValueAsString(user);

			StringEntity entity = new StringEntity(jsonInString, ContentType.APPLICATION_JSON);
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost request = new HttpPost(REST_URL);
			request.setEntity(entity);

			HttpResponse response = httpClient.execute(request);
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String res = bufReader.readLine();
			ResponseCode responseCode = ResponseCode.valueOf(res);

			System.out.println(responseCode.getMessage());

			if (res.equalsIgnoreCase("res11")) {
				return true;
			} else {
				return false;
			}

		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}

	}
}
