package org.gamesys.registration.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.gamesys.registration.service.exclusion.ExclusionImpl;
import org.gamesys.registration.util.ValidationException;
/*
 * Validator has methods to validate the user details as per the specification and requirements. 
 * It will make a use of ExclusionService to validate ssn and date of birth.
 * In case of validation failure ValidationException will be thrown with appropriate message for failure.
 * 
 * */
public class Validator {

	private Pattern pattern;
	private Matcher matcher;
	private final String USERNAME_PATTERN = "^[a-zA-Z0-9]{3,}$"; //Username regex pattern: Minimum length 3,Only alphanumeric characters, no space or other characters.
	private final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[A-Z]).{4,})";//Password regex pattern: Minimum length 4, at least one uppercase letter and digit.
	private final SimpleDateFormat ISO8601FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

	public String validateUser(User user) throws ValidationException {

		if (validateUsername(user.getUsername()) && validatePassword(user.getPassword()) && validateDob(user.getDob())
				&& validateSsn(user)) {
			return "res11";
		} else {
			return "res07";
		}

	}

	private boolean validateSsn(User user) throws ValidationException {

		if ((0 != user.getSsn()) && (Integer.toString(user.getSsn()).length() == 8)) {
			List<User> registeredUsers = UserList.getInstance().getRegisteredUsers();
			for (User userTmp : registeredUsers) {
				if (userTmp.getSsn() == user.getSsn()) {
					throw new ValidationException("res05");
				}
			}
			ExclusionImpl exclusion = new ExclusionImpl();
			if (exclusion.validate(ISO8601FORMAT.format(user.getDob()), Integer.toString(user.getSsn()))) {
				return true;
			} else {
				throw new ValidationException("res06");
			}

		} else {
			throw new ValidationException("res04");
		}

	}

	private boolean validateDob(Date dob) throws ValidationException {
		Date today = new Date();
		if (null != dob && dob.compareTo(today) < 0) {
			return true;
		} else {
			throw new ValidationException("res03");
		}

	}

	private boolean validatePassword(String password) throws ValidationException {
		if (null != password && !("".equalsIgnoreCase(password.trim()))) {
			pattern = Pattern.compile(PASSWORD_PATTERN);
			matcher = pattern.matcher(password);
			if (matcher.matches()) {
				return true;
			} else {
				throw new ValidationException("res02");
			}
		} else {
			throw new ValidationException("res02");
		}
	}

	private boolean validateUsername(String username) throws ValidationException {
		if (null != username && !("".equalsIgnoreCase(username.trim()))) {
			pattern = Pattern.compile(USERNAME_PATTERN);
			matcher = pattern.matcher(username);
			if (matcher.matches()) {
				return true;
			} else {
				throw new ValidationException("res01");
			}
		} else {
			throw new ValidationException("res01");
		}

	}

}
