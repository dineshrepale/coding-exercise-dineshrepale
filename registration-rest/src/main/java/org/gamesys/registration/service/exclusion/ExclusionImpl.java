package org.gamesys.registration.service.exclusion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.gamesys.registration.util.StubbedData;
/*
 * It is implementation for Exclusion service. It provides the logic for validating ssn and Date of birth.
 * It also loads stubbed data which used to validate the data.
 * 
 * */
public class ExclusionImpl implements ExclusionService {

	private static Map<String, String> ssnInfo = new HashMap<String, String>();
	private static List<String> blackListedSsn = new ArrayList<String>();

	@Override
	public boolean validate(String dob, String ssn) {

		if (null == ssnInfo.get(ssn) || !(ssnInfo.get(ssn).equalsIgnoreCase(dob)) || blackListedSsn.contains(ssn)) {
			return false;
		} else {
			return true;
		}

	}

	public ExclusionImpl() {
		ssnInfo = StubbedData.getSsnInfo();
		blackListedSsn = StubbedData.getBlackListedSsn();
	}
}
