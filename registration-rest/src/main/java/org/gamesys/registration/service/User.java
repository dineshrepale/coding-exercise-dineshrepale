package org.gamesys.registration.service;

import java.util.Date;
import org.gamesys.registration.util.JsonDateDeserializer;
import org.gamesys.registration.util.JsonDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
/*
 * User is entity class, Simple POJO with fields and getter-setter.
 * 
 * 
 * */
public class User {
	String username;
	String password;
	@JsonDeserialize(using = JsonDateDeserializer.class)
	Date dob;
	int ssn;

	public User() {

	}

	public User(String username, String password, Date dob, int ssn) {
		this.username = username;
		this.password = password;
		this.dob = dob;
		this.ssn = ssn;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@JsonSerialize(using = JsonDateSerializer.class)
	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public int getSsn() {
		return ssn;
	}

	public void setSsn(int ssn) {
		this.ssn = ssn;
	}

}
