package org.gamesys.registration.service.exclusion;
/*
 * Exclusion service provides the linkage to validate ssn and date of birth.
 * 
 * */
public interface ExclusionService {

	boolean validate(String dob, String ssn);

}
