package org.gamesys.registration.service;

import java.util.List;

/*
 * This is RESTService class, Registration class is the starting point of registration service.
 * It will receive input data in Json format, Same will be validated and appropriate string responsecode will be reverted to client.
 * 
 * */

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.gamesys.registration.util.ValidationException;
/*
 * This is the start point of RegistrationService. It receives data in Json format.
 * Details will be validated and according to ValidatioException or success scenario appropriate responsecode in string format will be returned to client.
 *  
 * 
 * */
@Path("/register")
public class Registration {

	Validator validator = new Validator();
	List<User> registredeUsers;

	@SuppressWarnings("finally")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String register(User user) {
		

		String responseCode = null;
		try {
			responseCode = validator.validateUser(user);
			if (responseCode.equalsIgnoreCase("res11")) {
				addUser(user);
			}

		} catch (ValidationException e) {
			e.printStackTrace();
			responseCode = e.getMessage();
		} catch (Exception e){
			e.printStackTrace();
			responseCode = "res20";
		}finally {
			return responseCode;
		}

	}

	private void addUser(User user) {
		registredeUsers = UserList.getInstance().getRegisteredUsers();
		registredeUsers.add(user);

	}

}
