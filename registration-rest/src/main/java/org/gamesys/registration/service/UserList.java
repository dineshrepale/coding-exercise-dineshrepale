
package org.gamesys.registration.service;

import java.util.ArrayList;
import java.util.List;
/*
 * UserList class is implemented with singleton pattern to behave as a in-memory to hold data.
 * It is the storage unit for the registration service.
 * 
 * */
public class UserList {

	private static UserList userList = null;

	public List<User> registeredUsers;

	public List<User> getRegisteredUsers() {
		return registeredUsers;
	}

	public void setRegisteredUsers(List<User> registeredUsers) {
		this.registeredUsers = registeredUsers;
	}

	private UserList() {
		registeredUsers = new ArrayList<User>();
	}

	public static UserList getInstance() {
		if (userList == null)
			userList = new UserList();

		return userList;
	}
}
