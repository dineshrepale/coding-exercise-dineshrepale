package org.gamesys.registration.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
/*
 * It is an Application class for maven project.
 * 
 * */
@ApplicationPath("restapi")
public class RestApiApp extends Application {

}
