package org.gamesys.registration.util;
/*
 * It is enum class used for properties and response messages.
 * 
 * */
public enum ResponseCode {

	res01("Username is incorrect (Only alphanumeric characters allowed)."),
	res02("Password does not complaint with password policy."), 
	res03("Date of birth cann't be greater than today."),
	res04("Incorrect SSN (Social Security Number), It should be of 8 digit."), 
	res05("User is already registered."),
	res06("Incorrect SSN and Date of birth or SSN is blacklisted."), 
	res07("Validation failed."),
	res11("User registered successfully."),
	res20("Something gone wrong.");

	private String message;

	private ResponseCode(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
