package org.gamesys.registration.util;
/*
 * Custom exception is created for validation failure cases. Which will contains message regarding the failure case.
 * 
 * */
public class ValidationException extends Exception {

	private static final long serialVersionUID = 1L;

	public ValidationException(String message) {
		super(message);
	}

}
