package org.gamesys.registration.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
/*
 * JsonDateDeserializer Overrides deserialize() method of StdDeserializer and provides provision to deserialize the Date field in Json.
 * 
 * 
 * */
public class JsonDateDeserializer extends StdDeserializer<Date> {

	
	private static final long serialVersionUID = 1L;

	public JsonDateDeserializer() {
		this(null);
	}

	protected JsonDateDeserializer(Class<?> clazz) {
		super(clazz);
	}

	@Override
	public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		String date = jp.getText();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			return sdf.parse(date);
		} catch (Exception e) {
			return null;
		}

	}

}
