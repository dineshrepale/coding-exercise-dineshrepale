package org.gamesys.registration.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
/*
 * This class has been created to just provide the data through ExclusionService.
 * The data is created randomly and used for validation. 
 * 
 * 
 * */
public class StubbedData {

	public static Map<String, String> getSsnInfo() {
		Map<String, String> ssnInfoMap = new HashMap<String, String>();

		ssnInfoMap.put("67722540", "2002-12-09T00:00:00Z");
		ssnInfoMap.put("64373042", "2008-01-24T00:00:00Z");
		ssnInfoMap.put("29117175", "1999-01-21T00:00:00Z");
		ssnInfoMap.put("54265927", "1973-05-31T00:00:00Z");
		ssnInfoMap.put("99892522", "1980-10-09T00:00:00Z");
		ssnInfoMap.put("03034970", "1982-06-02T00:00:00Z");
		ssnInfoMap.put("13616105", "1972-02-23T00:00:00Z");
		ssnInfoMap.put("03850741", "1996-04-30T00:00:00Z");
		ssnInfoMap.put("86949179", "1987-05-19T00:00:00Z");
		ssnInfoMap.put("80002028", "1982-10-20T00:00:00Z");
		ssnInfoMap.put("51917269", "1976-07-13T00:00:00Z");
		ssnInfoMap.put("01741683", "1999-10-08T00:00:00Z");
		ssnInfoMap.put("71406188", "1975-04-03T00:00:00Z");
		ssnInfoMap.put("79159932", "1984-08-27T00:00:00Z");
		ssnInfoMap.put("42999393", "1978-11-01T00:00:00Z");
		ssnInfoMap.put("96302157", "2007-12-03T00:00:00Z");
		ssnInfoMap.put("50687393", "1976-05-06T00:00:00Z");
		ssnInfoMap.put("63494239", "1998-08-31T00:00:00Z");
		ssnInfoMap.put("14047319", "2001-10-08T00:00:00Z");
		ssnInfoMap.put("90798671", "1999-08-22T00:00:00Z");
		ssnInfoMap.put("81240688", "1970-06-04T00:00:00Z");
		ssnInfoMap.put("94299766", "1979-04-18T00:00:00Z");
		ssnInfoMap.put("50609141", "1981-07-17T00:00:00Z");
		ssnInfoMap.put("83233060", "1979-02-01T00:00:00Z");
		ssnInfoMap.put("36289815", "2014-02-14T00:00:00Z");
		ssnInfoMap.put("34408929", "1990-01-04T00:00:00Z");
		ssnInfoMap.put("86927827", "1980-09-26T00:00:00Z");
		ssnInfoMap.put("66316786", "2000-06-07T00:00:00Z");
		ssnInfoMap.put("73169700", "2009-06-11T00:00:00Z");
		ssnInfoMap.put("05854697", "1994-06-03T00:00:00Z");
		ssnInfoMap.put("55338775", "1995-02-06T00:00:00Z");
		ssnInfoMap.put("22211368", "1984-09-02T00:00:00Z");
		ssnInfoMap.put("70572422", "1970-04-30T00:00:00Z");
		ssnInfoMap.put("17536824", "2014-11-05T00:00:00Z");
		ssnInfoMap.put("33515607", "1971-01-10T00:00:00Z");
		ssnInfoMap.put("16123479", "2005-11-13T00:00:00Z");
		ssnInfoMap.put("80330182", "2001-12-14T00:00:00Z");
		ssnInfoMap.put("38360809", "1973-09-17T00:00:00Z");
		ssnInfoMap.put("53308272", "1999-03-04T00:00:00Z");
		ssnInfoMap.put("68456758", "1998-10-17T00:00:00Z");

		return ssnInfoMap;
	}

	public static List<String> getBlackListedSsn() {
		Set<String> blackListedSsnSet = new HashSet<String>();

		blackListedSsnSet.add("03558417");
		blackListedSsnSet.add("33915922");
		blackListedSsnSet.add("53741256");
		blackListedSsnSet.add("72576318");
		blackListedSsnSet.add("07320246");
		blackListedSsnSet.add("34520698");
		blackListedSsnSet.add("51678417");
		blackListedSsnSet.add("34688388");
		blackListedSsnSet.add("76432263");
		blackListedSsnSet.add("24360827");
		blackListedSsnSet.add("07512213");
		blackListedSsnSet.add("53064630");
		blackListedSsnSet.add("84628697");
		blackListedSsnSet.add("17229987");
		blackListedSsnSet.add("53390823");
		blackListedSsnSet.add("11777033");
		blackListedSsnSet.add("90896668");
		blackListedSsnSet.add("19856462");
		blackListedSsnSet.add("99968459");
		blackListedSsnSet.add("74277587");
		blackListedSsnSet.add("89570279");
		blackListedSsnSet.add("47682020");
		blackListedSsnSet.add("34533850");
		blackListedSsnSet.add("35549173");
		blackListedSsnSet.add("57139934");
		blackListedSsnSet.add("83934554");
		blackListedSsnSet.add("22849592");
		blackListedSsnSet.add("04957230");
		blackListedSsnSet.add("85704720");
		blackListedSsnSet.add("75372644");
		blackListedSsnSet.add("12769136");
		blackListedSsnSet.add("62614934");
		blackListedSsnSet.add("08648603");
		blackListedSsnSet.add("75545083");
		blackListedSsnSet.add("71088609");
		blackListedSsnSet.add("16639408");
		blackListedSsnSet.add("62506018");
		blackListedSsnSet.add("02501050");
		blackListedSsnSet.add("30070232");
		blackListedSsnSet.add("15618669");

		return new ArrayList<String>(blackListedSsnSet);
	}

}
