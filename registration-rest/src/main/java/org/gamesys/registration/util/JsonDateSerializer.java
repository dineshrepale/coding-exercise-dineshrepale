package org.gamesys.registration.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
/*
* JsonDateSerializer Overrides serialize() method of JsonSerializer and provides provision to serialize the Date field in Json.
* 
* 
* */
public class JsonDateSerializer extends JsonSerializer<Date> {

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public void serialize(Date date, JsonGenerator jgen, SerializerProvider provider)
			throws IOException, JsonProcessingException {

		String formattedDate = sdf.format(date);
		jgen.writeString(formattedDate);

	}

}
